import React from 'react';
import {AsyncStorage, Alert, StyleSheet, Text, View ,YellowBox,StatusBar} from 'react-native';
import {createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import {Footer, FooterTab, Spinner,Container,Content,Header,Button,Icon} from 'native-base'
//import _confirm_logout from '../Indexo'
import {setZez} from '../rdx/akzion';
import * as Expo from 'expo'
import {connect} from 'react-redux'
import FormData from 'FormData';


const mapStateToProps = state => {
  return {
      base : {...state.base}
  }
}

const mapDispactToProps = dispatch => {
  return {
      setZez : data => dispatch(setZez(data))
  }
}

export const _delete_all_data = async(props)=>{
  await AsyncStorage.removeItem('Sess')
  .then(res => {
      return props.navigation.navigate('Loin');
  })
}



class Teras extends React.Component {
  constructor(props){
    super(props)
      this.state = {
        munyer:true
      }
  }

  async componentWillMount(){
    await Expo.Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    await this._check_is_login()
    this.setState({munyer:false})

};

  _check_is_login = async() => {
    try{
        const val = await AsyncStorage.getItem('Sess')
        .then((res)=>{
            if(res !== null){
  
  //---------------------Intent nk Dashboard-----------------------------
                //this.props.saveLoginData(val)
                this.props.setZez(val)
                const navigate = this.props.navigation
                return navigate.navigate('Indexo')
  //--------------------------------------------------------------------- 
  
            }else{
                console.log('tidak ditemukan!')
            }
        })
        
    }catch (err){
        console.log(err)
    }
  }
  

  render() {
    let munyers = this.state.munyer
        if(munyers){
            return ( 
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Spinner color='green' />
                    <Text>Loading...</Text>
                </View>
            )
        }
    return (
      <Container>
       
        <Content>
          
        </Content>
        <Footer>
                <FooterTab >
                        <Button  style={{backgroundColor:'#006999'}}
                             onPress={()=>this.props.navigation.navigate('Upzy')}>
                              <Text style={{color:'#fff'}}>Daftar</Text>
                        </Button>
                        <Button  style={{backgroundColor:'#72ae59'}}
                             onPress={()=>this.props.navigation.navigate("Loin")}>
                              <Text style={{color:'#fff'}} >Masuk</Text>
                        </Button>
                    </FooterTab>
                </Footer>
      </Container>
    );
  }
}
const Teracota = connect(mapStateToProps,mapDispactToProps)(Teras)
export default Teracota;

/*YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
