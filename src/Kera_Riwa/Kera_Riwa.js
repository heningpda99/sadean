import React from 'react';
import { StyleSheet, Text, View ,YellowBox,StatusBar} from 'react-native';
import {createMaterialTopTabNavigator,createBottomTabNavigator,createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import Keranjang from './Kera_Riwa_Child/Keranjang';
import Riwayat from './Kera_Riwa_Child/Riwayat';
// import Icon from 'react-native-vector-icons/Ionicons';
// import {Header} from 'react-native-elements';
import {Button,Container,Content,Tabs,Header,Icon,Right,Left,Body} from 'native-base';

class Kera_Riwa extends React.Component {
  // static navigationOptions={
  //       title:'Riwayat',
  //       //header:null
  // }
  render() {
    return (
      // <View style={styles.container}>
      //   <Text>Open up App.js to start working on your app!</Text>
      // </View>
      
      <Container>
        {/* <Header style={{backgroundColor:'#fff'}}>
          <Right>
            <Button transparent>
            <Icon name='ios-search' style={{color:'#fff'}}/>
            </Button>
          </Right>
          <Body>
            <View></View>
          </Body>
          <Left>
            <View></View>
          </Left>
        </Header> */}
         <Omee/>
      </Container>
    );
  }
}

const Atb = createMaterialTopTabNavigator({
  Keranjang:{screen:Keranjang,
        navigationOptions:{
          tabBarLabel:'Keranjang',
          tabBarIcon:({tintColor})=>(
            <Icon name='ios-home' color={tintColor} size={24}/>
          )
        }},
  Riwayat:{screen:Riwayat,
          navigationOptions:{
            tabBarLabel:'Riwayat',
            tabBarIcon:({tintColor})=>(
              <Icon name='ios-person' color={tintColor} size={24}/>
            )}
    }
},{
  navigationOptions:{
    //routes e Halaman
    initialRouteName:'Keranjang',
    order:['Keranjang','Riwayat'],
    //ngatur tab e nduwur barang
    tabBarVisible:true,
  },
  tabBarOptions:{
    activeTintColor:'#A3356A',
    inactiveTintColor:'grey',
    showLabel:true,
    showIcon:false,
    style:{
      backgroundColor:'#fff'
    }
  },
    tabBarSelectedItemStyle: {
      borderBottomWidth: 2,
      borderBottomColor: '#A3356A',
  },
});

const JAV = createStackNavigator({
  Atb
},{
 //nuliso opo kene
})

const Omee = createAppContainer(Atb);
export default Kera_Riwa;



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
