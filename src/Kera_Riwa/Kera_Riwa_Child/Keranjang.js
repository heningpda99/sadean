import React from 'react';
import { Alert, Dimensions, StyleSheet, Text, View ,YellowBox} from 'react-native';
import {createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import {Thumbnail, Button,Container,Header,Content,Icon,Right,Left,Body,List,ListItem,Card,CardItem} from 'native-base'
import {connect} from 'react-redux';
import {setZez,setNext,setFiltre,setQuan,addToCart,setRelocate,removeItem, increaseQuantity} from '../../rdx/akzion'
import * as Expo from 'expo'
import {Entypo, Ionicons} from '@expo/vector-icons';
//import { toal } from '../../rdx/cartReducer';

const mapStateToProps = state => {
  return {
      base : {...state.norm.base},
      profile:{...state.norm.profile},
      //meta:{...state.meta},
      filtreo:{...state.norm.filtreo},

      cart:{...state.true_cart.cart},
      total:{...state.true_cart.total},
      //qty:{...state.qty}

  }
}

const mapDispactToProps = dispatch => {
  return {
      setZez : data => dispatch(setZez(data)),
      //setNext : data =>dispatch(setNext(data)),
      setFiltre: data =>dispatch(setFiltre(data)),
      setQuan: data =>dispatch(setQuan(data)),
      addToCart: data =>dispatch(addToCart(data)),

      setRelocate: data =>dispatch(setRelocate(data)),
      removeItem :data =>dispatch(removeItem(data)),
      increaseQuantity:data =>dispatch(increaseQuantity(data))
  }
}

// const mapStateToProps = (state) => ({
//       base : state.base,
//       profile: state.profile,
//       //meta:{...state.meta},
//       filtreo:state.filtreo,

//       cart:state.cart,
//       total:state.total,
//       //qty:state.kree.qty,
// })


class Keranjang extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      crt:[],
      teot:0,

      habi:0
    }
  
  }
  async componentWillMount(){
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
  });
  }
  
  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.cart !== this.props.cart) {
  //       //this.startAnimation();
  //       this.setState({crt:this.props.cart})
        
  //   }
  // }

  componentDidMount(){
   
      const { cart } = this.props;
      const total = Object.values(cart).reduce((acc, item) => {
         acc +=   item.topri
         return acc
        }, 0)
      //return total.toFixed(2)  

     
      this.willFocusSubscription = this.props.navigation.addListener(
       'willFocus',
       () => { 
            const { teot } = this.state;
                      this.setState({
                              teot: total.toFixed(2)
                          })
       })

  }

  componentWillUnmount(){

    this.willFocusSubscription.remove();

  }

  Batallion(item){
    try{
      const io = item;
      //console.warn(io)
      this.props.removeItem(io)
      //const { totalCost } = this.props
       const { cart } = this.props
        console.warn(cart)  
        //console.warn(totalCost)    

    }catch(err){
      alert(err)
    }
  }

  Increasee(item){
    try{
      const { teot } = this.state;
      console.warn(item)
        this.props.increaseQuantity(item)
      
       

    }catch(err){
      alert(err);
    }
  }
  

  render() {
    var data = this.props.cart
    //var toal = this.props.total
    //const { totalCost } = this.props 
    return (
      
      <Container>
         <Content> 
            <List dataArray={data} horizontal={false}
                          renderRow={(item) => 
                                <ListItem thumbnail>
                                   {/* <Left>
                                   </Left> */}
                                    <Body>
                                          <View style={{flexDirection:'row'}}>
                                          <View style={{ flexDirection:'column'}}>
                                            <Text style={{padding:10}}>{item.nama_produk}</Text>
                                            <Text style={{padding:10}}>{item.topri}</Text>                                     
                                          </View>
                                            <View style={{flexDirection:'row',padding:10}}>
                                              <Entypo size={25} name="squared-minus"></Entypo>
                                                <Text style={{padding:5}}>{item.qua}</Text> 
                                              <Entypo size={25} name="squared-plus" onPress={()=>this.Increasee(item)}></Entypo>
                                            </View>
                                            
                                          </View>
                                    </Body>
                                    <Right>
                                      <Button danger style={{padding:10}} onPress={()=>this.Batallion(item)}>
                                        <Text style={{color:"#fff"}}>Cancel</Text>
                                      </Button>
                                    </Right>
                                </ListItem>

                               
                        }>
            </List>
         </Content> 
            <View style={{flexDirection:'row'}}>
                        <Text>Totalmu : {this.state.teot}</Text>
            </View>
      </Container>
                        // <View>
                        //          <View style={{flex:1,flexDirection:'row',width:Dimensions.get('screen').width}}>
                        //            <View>
                        //              <Text>{mite.nama_produk}</Text>
                        //            </View>
                        //            <View>
                        //                <Button transparent>
                        //                    <Entypo size={22} name="squared-minus"></Entypo>
                        //                </Button>
                        //              <Text>{mite.qua}</Text>
                        //                <Button transparent>
                        //                    <Entypo size={22} name="squared-plus"></Entypo>
                        //                </Button>
                        //            </View>
                        //            <View>
                        //              <Text>{mite.qua * mite.harga_biji}</Text>
                        //            </View>
                        //            <View>
                        //                <Button danger>
                        //                  <Text style={{color:"#fff"}}>Batal</Text>
                        //                </Button>
                        //            </View>

                        //          </View>
                        // </View>        
    );
  }
}
//const Omee = createAppContainer(createStackNavigator({Home}));
const Kree = connect(mapStateToProps,mapDispactToProps)(Keranjang)
export default Kree;

/*YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
