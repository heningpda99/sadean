import React from 'react';
import {Dimensions,AsyncStorage, Alert, StyleSheet, Text, View ,YellowBox,StatusBar} from 'react-native';
import {createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import {Container,Content,Header,Button,Icon} from 'native-base'
//import _confirm_logout from '../Indexo'

export const _delete_all_data = async(props)=>{
  await AsyncStorage.removeItem('Sess')
  .then(res => {
      return props.navigation.navigate('Loin');
  })
}



class Profile extends React.Component {
  constructor(props){
    super(props)

  }

  _confirm_logout(props){
    Alert.alert(
        'Konfirmasi',
        'Apakah Anda Yakin?',
        [
          {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Ya', onPress: () => _delete_all_data(props)},
        ],
        { cancelable: true }
      )
  }

  render() {
    return (
      <Container>
        <Header style={{height:Dimensions.get('screen').width/5,
              backgroundColor:'#fff'}}>

        </Header>
        <Content>
          <Button block border alert onPress={()=>this._confirm_logout(this.props)}>
            <Icon name='ios-exit'/>
          </Button>
        </Content>
      </Container>
    );
  }
}
// const Ofil = createAppContainer(createStackNavigator({Profile}));
export default Profile;

/*YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
