import React from 'react';
import { AsyncStorage, Dimensions, StyleSheet, View ,YellowBox} from 'react-native';
import {createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import {Toast,Text, Spinner, Body, Container,Header,Content, Form,Input,Item, Label,Button,Card,CardItem} from 'native-base'
import axios from 'axios';
import {setZez} from '../rdx/akzion';
import * as Expo from 'expo'
import {connect} from 'react-redux'
import FormData from 'FormData';


const mapStateToProps = state => {
  return {
      base : {...state.base}
  }
}

const mapDispactToProps = dispatch => {
  return {
      setZez : data => dispatch(setZez(data))
  }
}
class Masok extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      munyer : true,
      email : "",
      password: ""
    }
  }

  async componentWillMount(){
    await Expo.Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    await this._check_is_login()
    this.setState({munyer:false})
};


  Mlebu = async() => {
    const base = this.props.base
    const url = base.UerEl+base.base_path.login
    var data = {
        email: this.state.email,
        password: this.state.password
       };
    var formdata = new FormData();
    formdata.append('email',this.state.email);
    formdata.append('password',this.state.password);   
       try {
        let response = await fetch(url,
         {
           method: 'POST',
           
          body: formdata
         
        }
       ).then(response=>{
           console.log(response.ok);
           if(response.ok){
              let data=response._bodyText;
              let json=JSON.parse(data);
               console.log(json);
               this.setState({munyer:true})
            this._save_data_login(json);
           }else{
               this.setState({munyer:false})
            Toast.show({
                text: 'Username dan Password Masih Belum Benar',
                type:'danger'
            })
           }
       });
      } catch (errors) {
          
        alert(errors);
       } 
}


_save_data_login = async(data) => {
    // console.log(data)
    try{
        const _data = JSON.stringify(data)
        //console.log(_data)
        await AsyncStorage.setItem('Sess',_data)
        .then(()=>{
            //this.props.saveLoginData(data)
            this.props.setZez(data)
            const navigate = this.props.navigation
            this.setState({munyer:false})
            return navigate.navigate('Indexo')
        })
    }catch(err){
        this.setState({munyer:false})
        alert(arr)
    }
}


_check_is_login = async() => {
  try{
      const val = await AsyncStorage.getItem('Sess')
      .then((res)=>{
          if(res !== null){

//---------------------Intent nk Dashboard-----------------------------
              //this.props.saveLoginData(val)
              this.props.setZez(val)
              const navigate = this.props.navigation
              return navigate.navigate('Indexo')
//--------------------------------------------------------------------- 

          }else{
              console.log('tidak ditemukan!')
          }
      })
      
  }catch (err){
      console.log(err)
  }
}

  render() {

    let munyers = this.state.munyer
        if(munyers){
            return ( 
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Spinner color='green' />
                    <Text>Loading...</Text>
                </View>
            )
        }


    return (
      <Container>
        <Header >

        </Header>
        <Content contentContainerStyle={styles.container}>
          <Form style={{width:Dimensions.get('screen').width}}>
            <Item>
              <Input rounded  ref="email" placeholder="Emailmu" onChangeText = {(value)=>this.setState({email:value})} />
            </Item>
            <Item >
              <Input rounded ref="password" placeholder="Passwordmu" onChangeText = {(value)=>this.setState({password:value})}/>
            </Item>
              <Button block success rounded style={{margin:10}} onPress={()=>this.Mlebu()}>
                <Text style={{color:'#fff'}}>Masuk</Text>
              </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
// const Omee = createAppContainer(createStackNavigator({Home}));
const In = connect(mapStateToProps,mapDispactToProps)(Masok)

export default In;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'column'
  },

});
