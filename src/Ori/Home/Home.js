import React from 'react';
import { Dimensions,ScrollView,StyleSheet, Text, View ,YellowBox,StatusBar} from 'react-native';
import {createMaterialTopTabNavigator,createBottomTabNavigator,createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
//import Icon from 'react-native-vector-icons/Ionicons';
//import {Header} from 'react-native-elements';
import {Left,Button,Container,Header,Content,Icon,Right,Body,List,ListItem} from 'native-base'
import {
  RkText,
  RkButton,
  RkStyleSheet,
} from 'react-native-ui-kitten';
import { FontIcons } from '../../assets/icons';
import * as Screens from './gbhn_menu/index_menu';
import Slideshow from 'react-native-image-slider-show';
import HaPenDuk from './HaPenDuk/HaPenDuk';


const Arahan = [
  {
    id: 'Sayur',
    title: 'Sayuran',
    icon: FontIcons.login,
    screen: Screens.Sayur,
  },{
    id: 'Buah',
    title: 'Buah-Buahan',
    icon: FontIcons.login,
    screen: Screens.Buah,
  },{
    id: 'Daging',
    title: 'Daging',
    icon: FontIcons.login,
    screen: Screens.Daging,
  }
  ,{
    id: 'Bumbu',
    title: 'Bumbu',
    icon: FontIcons.login,
    screen: Screens.Bumbu,
  }
]

var coba =[
  {
    icon:'ios-options',
    name:'Sayur'
  },
  {
    icon:'ios-options',
    name:'Buah'
  },
  {
    icon:'ios-options',
    name:'Daging'
  },
  {
    icon:'ios-options',
    name:'Sembako'
  },

]

let wihi= Dimensions.get('window')

class Home extends React.Component {
  constructor(props){
    super(props)
    this.state = {
        dimensions:undefined
    }
    this.state = {
      position: 1,
      interval: null,
      dataSource: [
        {
          title: 'Title 1',
          caption: 'Caption 1',
          url: 'http://placeimg.com/640/480/any',
        }, {
          title: 'Title 2',
          caption: 'Caption 2',
          url: 'http://placeimg.com/640/480/any',
        }, {
          title: 'Title 3',
          caption: 'Caption 3',
          url: 'http://placeimg.com/640/480/any',
        },
      ],
    };
}

componentWillMount() {
  this.setState({
    interval: setInterval(() => {
      this.setState({
        position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
      });
    }, 2000)
  });
}

componentWillUnmount() {
  clearInterval(this.state.interval);
}




  render() {
    return (
             

         <Container>
           <Header style={{height:Dimensions.get('screen').width/5,
              backgroundColor:'#fff'}}>
            <Left style={{marginTop:20}}>
              <View></View>
            </Left>
            <Body style={{marginTop:20}}>
              <Text style={{color:'#fe4c4c',fontSize:17,fontWeight:'bold'}}> Tek-ON</Text>
            </Body>
            <Right style={{marginTop:20}}>
              <View></View>
            </Right>
           </Header>
           <Content>
             <List>
               <ListItem>
                  <Slideshow 
                    dataSource={this.state.dataSource}
                    position={this.state.position}
                    onPositionChanged={position => this.setState({ position })} />
               </ListItem>
               <ListItem itemDivider>
               <Text>Menu</Text>
               </ListItem>
               <ListItem>
                 <View style={{flex:1, flexDirection:'column' }}>
                 <View style={{flexDirection:'row',flexWrap:'wrap',alignItems:'center', justifyContent:'center'}}>

                  <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                    <Button bordered danger 
                    style={{flexDirection:'column', width:wihi.width/6,margin:10,height:wihi.height/10}}
                    onPress={()=>this.props.navigation.navigate('Hub')}>
                      
                      <Icon name='ios-options'style={{margin:5}}/>
                      <Text style={{fontSize:8,margin:5}}>Filter</Text>
                    
                    </Button>
                    {/* <Button bordered danger 
                    style={{flexDirection:'column', width:wihi.width/6,margin:10,height:wihi.height/10}}>
                      
                      <Icon name='ios-options'style={{margin:5}}/>
                      <Text style={{fontSize:8,margin:5}}>Filter</Text>
                    
                    </Button>
                    <Button bordered danger 
                    style={{flexDirection:'column', width:wihi.width/6,margin:10,height:wihi.height/10}}>
                      
                      <Icon name='ios-options'style={{margin:5}}/>
                      <Text style={{fontSize:8,margin:5}}>Filter</Text>
                    
                    </Button>
                    <Button bordered danger 
                    style={{flexDirection:'column', width:wihi.width/6,margin:10,height:wihi.height/10}}>
                      
                      <Icon name='ios-options'style={{margin:5}}/>
                      <Text style={{fontSize:8,margin:5}}>Filter</Text>
                    
                    </Button> */}
                    
                    
                    </View>
                    </View>
                    </View>
               </ListItem>
               {/* <ListItem>
                 <View style={{flex:1,flexDirection:'column',flexWrap:'wrap'}}>
                  <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                   <View style={{backgroundColor:'#000',height:20,width:20}} />
                   <View style={{backgroundColor:'#000',height:20,width:20}} />
                  </View>
                 </View>
               </ListItem> */}
             </List>
           </Content>
         </Container>
    );
  }
}

// const Atb = createMaterialTopTabNavigator({
//   Dashboard:{screen:Dashboard,
//         navigationOptions:{
//           tabBarLabel:'Dashboard',
//           tabBarIcon:({tintColor})=>(
//             <Icon name='ios-home' color={tintColor} size={24}/>
//           )
//         }},
//   Riwayat:{screen:Riwayat,
//           navigationOptions:{
//             tabBarLabel:'Riwayat',
//             tabBarIcon:({tintColor})=>(
//               <Icon name='ios-person' color={tintColor} size={24}/>
//             )}
//     }
// },{
//   navigationOptions:{
//     //routes e Halaman
//     initialRouteName:'Dashboard',
//     order:['Dashboard','Riwayat'],
//     //ngatur tab e nduwur barang
//     tabBarVisible:true,
//   },
//   tabBarOptions:{
//     activeTintColor:'green',
//     inactiveTintColor:'grey',
//     showLabel:true,
//     showIcon:false,
//     style:{
//       backgroundColor:'white'
//     }
//   },
//     tabBarSelectedItemStyle: {
//       borderBottomWidth: 2,
//       borderBottomColor: 'red',
//   },
// });


const ASU = createAppContainer(createStackNavigator({
  Hom :{screen:Home},
  Hub :{screen :HaPenDuk}
},{
 //nuliso opo kene
 headerMode:'none'
}));


export default ASU;

/*YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);*/

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    // alignItems: 'center',
    //justifyContent: 'center',
    flex:1,
    flexDirection:'row',
    justifyContent:"space-around"
  },
});
