import React from 'react';
import { TextInput,TouchableOpacity,AsyncStorage, Dimensions, StyleSheet, View ,YellowBox} from 'react-native';
import {createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import {Left,Right,Toast,Text, Icon,Spinner, Body, Container,Header,Content, Form,Input,Item, Label,Button,Card,CardItem} from 'native-base'
import axios from 'axios';
import Places from '../Places/Places';
import {setZez} from '../rdx/akzion';
//import * as Expo from 'expo'
import {connect} from 'react-redux'
import FormData from 'FormData';
//import RNGooglePlaces from 'react-native-google-places';
//import firebase from 'react-native-firebase';
import * as Expo from 'expo'




const mapStateToProps = state => {
  return {
      base : {...state.base}
  }
}

const mapDispactToProps = dispatch => {
  return {
      setZez : data => dispatch(setZez(data))
  }
}
class Regis extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      munyer : true,
      NIK:"",
      username:"",
      email : "",
      password: "",
      alamat:"",
      lat:"",
      lng:"",
      phone:"",
      apikey:""
    }
  }

  async componentWillMount(){
    await Expo.Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    await this._check_is_login()
    this.setState({munyer:false})
};

async componentDidMount(){
  // var fcmToken = await firebase.messaging().getToken();
  // this.setState({
  //   apikey:fcmToken
  // })
  // console.warn(fcmToken)

}










// openSearchModal() {
//   RNGooglePlaces.openPlacePickerModal()
//   .then((place) => {
//   console.warn(place)
//   this.setState({
//     alamat:place.name,
//     lat:place.latitude,
//     lng:place.longitude
//   })
 
//   })
//   .catch(error => 
//     console.log(error.message));  // error is a Javascript Error object
// }

Daftar=()=>{
  const base = this.props.base
    const url1 = base.UerEl+base.base_path.regis;
    axios.post(url1,{
      
        NIK:this.state.NIK,
        username:this.state.username,
        email:this.state.email,
        password:this.state.password,
        alamat:this.state.alamat,
        lat:this.state.lat,
        lng:this.state.lng,
        phone:this.state.phone,
        apikey:this.state.apikey
      
    }).then((response)=>{
      console.warn(response)
    })
}





_save_data_daftar = async(data) => {
    // console.log(data)
    try{
        const _data = JSON.stringify(data)
        //console.log(_data)
        await AsyncStorage.setItem('Sess',_data)
        .then(()=>{
            //this.props.saveLoginData(data)
            this.props.setZez(data)
            const navigate = this.props.navigation
            this.setState({munyer:false})
            return navigate.navigate('Indexo')
        })
    }catch(err){
        this.setState({munyer:false})
        alert(arr)
    }
}


_check_is_login = async() => {
  try{
      const val = await AsyncStorage.getItem('Sess')
      .then((res)=>{
          if(res !== null){

//---------------------Intent nk Dashboard-----------------------------
              //this.props.saveLoginData(val)
              this.props.setZez(val)
              const navigate = this.props.navigation
              return navigate.navigate('Indexo')
//--------------------------------------------------------------------- 

          }else{
              console.log('tidak ditemukan!')
          }
      })
      
  }catch (err){
      console.log(err)
  }
}





//--------------------------------------Phone Number Verification---------------------------------------------------




  render() {

    let munyers = this.state.munyer
        if(munyers){
            return ( 
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
                    <Spinner color='green' />
                    <Text>Loading...</Text>
                </View>
            )
        }


    return (
      <Container>
        <Header style={{backgroundColor:'#006999'}} >
        <Left >
                    <Button transparent onPress={()=>this.props.navigation.navigate("Terace")}>
                    <Icon name="arrow-back" style={{color:'#fff'}}/>
                     </Button> 
                    </Left>
                    <Body>
                      <View></View>
                    </Body>
        </Header>
        <Content contentContainerStyle={styles.container}>
          <Form style={{width:Dimensions.get('screen').width}}>
            {/* <Item>
              <Input rounded  ref="NIK" placeholder="NIK" onChangeText = {(value)=>this.setState({NIK:value})} />
            </Item> */}
            {/* <Item>
              <Input rounded ref="username"  placeholder="Namamu" onChangeText = {(value)=>this.setState({username:value})} />
            </Item> */}
            <Item>
              <Input rounded ref="email" placeholder="Emailmu" onChangeText = {(value)=>this.setState({email:value})} />
            </Item>
            <Item >
              <Input rounded ref="password" secureTextEntry={true} placeholder="Passwordmu" onChangeText = {(value)=>this.setState({password:value})}/>
            </Item>
            <Item>
              <Input rounded ref="phone" placeholder="Nomor Hpmu" onChangeText = {(value)=>this.setState({phone:value})} />
            </Item>
            {/* <Item >
              <Input disabled ref="alamat" placeholder="Alamat" onChangeText={(value) => this.setState({ value })} value={this.state.alamat}/>
            </Item>
            <Item>
            <Text style={{color:'#72ae59',fontSize:10,fontWeight:'bold'}} onPress={()=>this.openSearchModal()}>Tekan untuk Dapatkan Alamat</Text>
            </Item> */}
            
            
              {/* <Button   normal block bordered  style={{marginTop:20}} onPress={()=>this.openSearchModal()}>
                <Text>Get Alamat</Text>
              </Button> */}
              
          </Form>
          <Button normal block transparent  style={{margin:20}} onPress={()=>this.Daftar()}>
                <Text >Register</Text>
              </Button>
        </Content>
      </Container>
    );
  }
}
// const Omee = createAppContainer(createStackNavigator({Home}));
const Up = connect(mapStateToProps,mapDispactToProps)(Regis)

export default Up;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'column'
  },

});
