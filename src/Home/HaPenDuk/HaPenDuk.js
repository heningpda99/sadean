import React from 'react';
import { TouchableHighlight, Modal,TouchableOpacity,Dimensions,ScrollView,StyleSheet, Text, View ,YellowBox,StatusBar,FlatList, Image} from 'react-native';
import {createMaterialTopTabNavigator,createBottomTabNavigator,createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import {Thumbnail, Button,Container,Header,Content,Icon,Right,Left,Body,List,ListItem,Card,CardItem} from 'native-base'
import {axios} from 'axios';
import {SearchBar} from 'react-native-elements'
import {connect} from 'react-redux'

import {setZez,setNext,setFiltre} from '../../rdx/akzion'
import ASU from '../Home';
import * as Expo from 'expo'


const mapStateToProps = state => {
  return {
      base : {...state.norm.base},
      profile:{...state.norm.profile},
      //meta:{...state.meta},
      filtreo:{...state.norm.filtreo},
  }
}

const mapDispactToProps = dispatch => {
  return {
      setZez : data => dispatch(setZez(data)),
      //setNext : data =>dispatch(setNext(data)),
      setFiltre: data =>dispatch(setFiltre(data))
  }
}


class HaPenDuk extends React.Component {
  constructor(props){
    super(props)
      this.state={
        is_loading:true,
        pagez:'',

        is_search:false,
        ketok:false,

        true_search:'',
        
        muncul_kasil:false,
        kasil_search:[],

        umper:[]
      }

      this.arrayholder = [];
        
    }

  async componentWillMount(){
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
  });
    this.setState({is_loading:false})
    var getString = this.props.navigation.getParam("titel");
    var GString = this.props.navigation.getParam("aidi");
    this.setState({pagez:'Cari '+getString+' Apa ?'})
  }

  componentDidMount(){
    this.UmPerjen();  
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
            //this.searchFilterFunction;    
            
          });
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();

  }

  UmPerjen(){
    var ikatz = encodeURIComponent(this.props.navigation.getParam("aidi"))
    var uer = this.props.base.UerEl+this.props.base.base_path.umum_perjenis+`?id_kategori=${ikatz}`;
    console.warn(uer);
    return fetch(uer,{
      method:"GET"
    }).then((response)=>{
      console.warn(response);
      this.setState({
        umper:JSON.parse(response._bodyText)
      })
    })
  }


  searchFilterFunction=true_search=>{
   //try{ 
        this.setState({true_search})
        var NamPro = encodeURIComponent(true_search.toUpperCase());
        var GString =encodeURIComponent(this.props.navigation.getParam("aidi"));
        //var NamPro = true_search.toUpperCase();
        //var GString = this.props.navigation.getParam("aidi");

        console.warn(GString)
        const base = this.props.base
        const url = base.UerEl+this.props.base.base_path.car_pro+`?id_kategori=${GString}&nama_produk=${NamPro}`;
        //const url = base.UerEl+this.props.base.base_path.car_pro;
        console.warn(url)
        return fetch(url,{
          method:"GET",
         
        }).then((response)=>{
          if(true_search!=""){
              console.warn(JSON.parse(response._bodyText))
                this.setState({
                    muncul_kasil:true,
                    is_loading: false,
                    kasil_search: JSON.parse(response._bodyText),
                  })
          }else{
              this.setState({kasil_search:[],muncul_kasil:false})
              
          }
        })  
        // return axios.get(url,{
        //     params:{
        //         id_kategori:GString,
        //         nama_produk:true_search.toUpperCase()
        //     }
           
        // }).then((response)=>{
        //     //if(true_search!=""){
        //       console.warn(response)
        //       this.setState({
        //         is_loading: false,
        //         kasil_search: response,
        //       })
              
        //       this.arrayholder = response;
        //     //}else{
              
        //     //}
        // })
           
     //}catch(err){

     //}
      
  }

  renderHeader(){
    const {true_search} = this.state;
    return(
      <SearchBar        
                placeholder="Mari Mencari ..."        
                lightTheme             
                onChangeText={this.searchFilterFunction}
                value={true_search}
                autoCorrect={false}             
              />  
    )
  }

  Modar=()=>{
    const base = this.props.base
    const {kasil_search} = this.state;
    return(
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.ketok}
          >
          <Container>
            <Header style={{backgroundColor:'#fff', elevation:0}}>
              <Left>
                <Icon style={{color:'#A3356A',fontSize:24,padding:5}} name='md-close' onPress={()=>this.setState({ketok:false})}/>
              </Left>
              <Body>
              </Body>
              {/* <Right><Icon style={{padding:5,color:'#A3356A',fontSize:19}} name="ios-options"/></Right> */}
            </Header>
            <Content>
            {this.renderHeader()}
              {/* <List> */}
               {/* <FlatList
                  style = {{ width: '100%' }}
                  keyExtractor = {( item, index ) => index }
                  data = { this.state.kasil_search }
                  renderItem = {({ item, index }) => 
                    
                  <ListItem thumbnail>
                      <Left>
                        <Thumbnail square source={{ uri: item.url_foto_produk }} />
                      </Left>
                      <Body>
                        <Text>{item.nama_produk}</Text>
                        <Text note numberOfLines={1}>{item.harga_biji}</Text>
                      </Body>
                      <Right>
                        <Button transparent>
                          <Text>View</Text>
                        </Button>
                      </Right>
                  </ListItem>
                                      
                   } /> */}
              {/* </List>     */}

              
                
                {(this.state.muncul_kasil)? kasil_search.map(function(item){
                  return(
                <List>   
                  <ListItem thumbnail>
                      <Left>
                        <Thumbnail square source={{ uri: base.UerGam+item.url_foto_produk }} />
                      </Left>
                      <Body>
                        <Text>{item.nama_produk}</Text>
                        <Text note numberOfLines={1}>{item.harga_biji}</Text>
                      </Body>
                      <Right>
                        {/* <Button transparent> */}
                        <Button success style={{padding:10}}>
                          <Text style={{color:"#fff"}}>Pesan</Text>
                        </Button>
                        {/* </Button> */}
                      </Right>
                  </ListItem>
                </List>    

                )
                 
                }):null
                //:
                  // <View style={{flex:1,backgroundColor: '#fff', flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                  //   <Text>Arep golek opo woe !!!</Text>
                  // </View>
                }
              
                

  
            </Content>
          </Container>
          {(!this.state.muncul_kasil)?
             <View style={{flex:2,backgroundColor: '#fff', flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
               <Text>Arep golek opo woe !!!</Text>
             </View>
             :null
            }
        </Modal>

      </View>
    )
  }


  Blak_Tampil(){
    const {umper} = this.state;
    const base = this.props.base
    return(
      <List>
        <ListItem itemDivider>
          <Text>Our Newest Fruit</Text>
        </ListItem>
        <List dataArray={umper} horizontal={true}
                          renderRow={(item) =>
                            <ListItem>
                            <Card>
                            {/* {umumz.map(function(item){return( */}
                                  {/* <View> */}
                                    <CardItem cardBody>
                                      <Image source={{uri: base.UerGam+item.url_foto_produk}} style={{height: Dimensions.get('screen').width/2, width: Dimensions.get('screen').width/2 }}/>
                                    </CardItem>
                                    <CardItem>
                                      
                                      <Body>
                                        <Text style={{fontWeight:'bold'}}>{item.nama_produk}</Text>
                                        <Text>{item.harga_biji}</Text>
                                        <Button success block style={{padding:10}}>
                                          <Text style={{color:'#fff'}}>Pesan</Text>
                                        </Button>
                                      </Body>
                                      
                                    </CardItem>
                                  {/* </View> */}
                                  {/* ) })} */}
                                
                            </Card>
                          </ListItem>

                          }>
        </List>
      </List>
    )
  }


  render() {
    
    return (
      <Container>
        <Header style={{backgroundColor:'#fff',elevation:0}}>
         
          <Body>
            <Button bordered block style={{color:'#A3356A'}}>
              <Left><Icon style={{padding:5, color:'#A3356A',fontSize:19}} name="arrow-back" onPress={()=>this.props.navigation.goBack()}/></Left>
              <Text style={{fontSize:12,color:'#bbbbbb',padding:5}} onPress={()=>this.setState({ketok:true})}>{this.state.pagez}</Text>
              <Right>
                {/* <Icon style={{padding:5,color:'#A3356A',fontSize:19}} name="ios-options"/> */}
              </Right>
            </Button>
          </Body>
          
        </Header>
        <Content>
            {this.Blak_Tampil()} 
            {this.Modar()}
        </Content>
      </Container>
    );
  }
}


const Hapens = connect(mapStateToProps,mapDispactToProps)(HaPenDuk) 
export default Hapens;



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textViewContainer: {
        
    textAlignVertical:'center', 
    padding:4,
    fontSize: 10,
    color: '#000000',
    
    },
    item:
        {
          padding: 10
        },
});
