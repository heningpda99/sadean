import React from 'react';
import { Alert,TouchableOpacity,Dimensions,ScrollView,StyleSheet, Text, View ,YellowBox,StatusBar,FlatList,Image} from 'react-native';
import {createMaterialTopTabNavigator,createBottomTabNavigator,createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
//import Icon from 'react-native-vector-icons/Ionicons';
//import {Header} from 'react-native-elements';
import {Button,Container,Header,Content,Icon,Right,Left,Body,List,ListItem,Card,CardItem,Title,Subtitle} from 'native-base'
import {
  RkText,
  RkButton,
  RkStyleSheet,
} from 'react-native-ui-kitten';
import { FontIcons } from '../../assets/icons';
import * as Screens from './gbhn_menu/index_menu';
import Slideshow from 'react-native-image-slider-show';
import Hapens from './HaPenDuk/HaPenDuk';
import {connect} from 'react-redux'
import * as Expo from 'expo'

import {setZez,setNext,setFiltre,setQuan,addToCart,setRelocate} from '../rdx/akzion'



const Arahan = [
  {
    id: 1,
    title: 'Sayuran',
    icon: 'ios-options',
    imageUrl:'http://via.placeholder.com/160x160',
    screen: Screens.Sayur,
  },{
    id: 2,
    title: 'Buah',
    icon: 'ios-options',
    imageUrl:'http://via.placeholder.com/160x160',
    screen: Screens.Buah,
  },{
    id: 5,
    title: 'Daging',
    icon: 'ios-options',
    imageUrl:'http://via.placeholder.com/160x160',
    screen: Screens.Daging,
  }
  ,{
    id: 6,
    title: 'Bumbu',
    icon: 'ios-options',
    imageUrl:'http://via.placeholder.com/160x160',
    screen: Screens.Bumbu,
  }
]

let wihi= Dimensions.get('window')

const mapStateToProps = state => {
  return {
      base : {...state.norm.base},
      profile:{...state.norm.profile},
      //meta:{...state.meta},
      filtreo:{...state.norm.filtreo},
      umum:{...state.norm.umum},
 
      cart:{...state.true_cart.cart},
      qty:{...state.true_cart.qty},
      total:{...state.true_cart.total}
  }
}

// const mapDispactToProps = (dispatch) => {
//   return {
//       addItemToCart: (product) => dispatch({ type: 'ADD_TO_CART', payload: product }),
//   }
// }

const mapDispactToProps = dispatch => {
  return {
      setZez : data => dispatch(setZez(data)),
      //setNext : data =>dispatch(setNext(data)),
      setFiltre: data =>dispatch(setFiltre(data)),
      setQuan: data =>dispatch(setQuan(data)),
      addToCart: (data,data1) =>dispatch(addToCart(data,data1)),

      setRelocate: data =>dispatch(setRelocate(data)),
      addItemToCart: (product) => dispatch({ type: 'Add_To_Cart', payload: product }),


  }
}

class Home extends React.Component {
  constructor(props){
    super(props)
    this.state = {
        dimensions:undefined,

        position: 1,
        interval: null,

        dataSource: [
          {
            title: 'Title 1',
            caption: 'Caption 1',
            url: 'http://placeimg.com/640/480/any',
          }, {
            title: 'Title 2',
            caption: 'Caption 2',
            url: 'http://placeimg.com/640/480/any',
          }, {
            title: 'Title 3',
            caption: 'Caption 3',
            url: 'http://placeimg.com/640/480/any',
          },
        ],

        umumz:[],

        qities:0,

        quantityById : 1
    }

}

async componentWillMount() {
  await Expo.Font.loadAsync({
    'Roboto': require('native-base/Fonts/Roboto.ttf'),
    'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
});
  this.setState({
    interval: setInterval(() => {
      this.setState({
        position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
      });
    }, 2000)
  });
}

componentDidMount(){
  this.Umumx();
   //const fuu = this.props.cart
   //console.warn(fuu)
}

componentWillUnmount() {
  clearInterval(this.state.interval);
}


Umumx(){
  let ur = this.props.base.UerEl+this.props.base.base_path.umum;
  console.warn(ur)
  return fetch(ur,{
    method:"GET"
  }).then((response)=>{
    console.warn(response);
    let yo = JSON.parse(response._bodyText)
    this.props.setRelocate(yo)
    this.setState({
      umumz:this.props.umum
    })
  })
}

AddCart(item){
   try{
            //console.warn(item) 
            //const quantity = parseInt(this.state.quantityById[item.id_produk]) || 1
            const quantity = 1
          
            //this.props.setQuan(quan);  
            this.props.addToCart(item,quantity);
            let ionic = this.props.cart;
            let tal = this.props.total;
            console.warn(ionic)        
            console.warn(tal)    

    }catch(err){
      alert(err)
    }
}




  render() {
    const {navigate} = this.props.navigation;
    const {umum} = this.props;
    const base = this.props.base

    return (
             
       <Container>
           <Header style={{backgroundColor:"fff",elevation:0.5}}>
              {/* <Left>
                <View></View>
              </Left> */}
              <Body>
                <Text style={{color:'#A3356A',fontSize:15,fontWeight:'bold'}}>Sadean</Text>
              </Body>
           </Header>
           <Content>
             <List>
             <Slideshow 
              dataSource={this.state.dataSource}
              position={this.state.position}
              onPositionChanged={position => this.setState({ position })} />
               
               <ListItem itemDivider>
               <Text style={{fontSize:20,color:'#000'}}>Mau Cari Produk Apa ?</Text>
               </ListItem>
               <ListItem>
                 <View style={{flex:1, flexDirection:'column' }}>
                 <View style={{flexDirection:'row',flexWrap:'wrap',alignItems:'center', justifyContent:'center'}}>

                  <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                   {Arahan.map(function(item){
                    return(
                      <Button bordered danger 
                      style={{flexDirection:'column', width:wihi.width/6,margin:10,height:wihi.height/10}} 
                       onPress={()=>navigate('Hub',{titel:item.title,aidi:item.id})}>
                        
                        <Icon name={item.icon}style={{margin:5}}/>
                        <Text style={{fontSize:8,margin:5}}>{item.title}</Text>
                      
                      </Button>
                    
                    )
                    })} 
                    

                    </View>
                    </View>
                    </View>
               </ListItem>
               <ListItem itemDivider>
                    
                  <Body> 
                      <Text style={{color:'#000',fontSize:10}}>Our Newest Products</Text>
                  </Body> 
                  <Right>
                      <Text style={{color:'#1ba602'}}>More</Text>
                  </Right>  
               </ListItem>
               <ListItem>
                      <List dataArray={umum} horizontal={true}
                          renderRow={(item) => 
              
                            <ListItem >
                              <Card>
                              {/* {umumz.map(function(item){return( */}
                                    {/* <View> */}
                                      <CardItem cardBody>
                                        <Image source={{uri: base.UerGam+item.url_foto_produk}} style={{height: Dimensions.get('screen').width/2, width: Dimensions.get('screen').width/2 }}/>
                                      </CardItem>
                                      <CardItem>
                                        
                                        <Body>
                                          <Text style={{fontWeight:'bold'}}>{item.nama_produk}</Text>
                                          <Text>{item.harga_biji}</Text>
                                          <Button success block style={{padding:10}} onPress={()=>this.AddCart(item)}>
                                            <Text style={{color:'#fff'}}>Pesan</Text>
                                          </Button>
                                        </Body>
                                        
                                      </CardItem>
                                    {/* </View> */}
                                    {/* ) })} */}
                                  
                              </Card>
                            </ListItem>
                        }>
                      </List>
               </ListItem>

             </List>
             
           </Content>
         </Container>
    );
  }
}
const Holme = connect(mapStateToProps,mapDispactToProps)(Home) 


const ASU = createAppContainer(createStackNavigator({
  Hom :{screen:Holme},
  Hub :{screen :Hapens}
},{
 //nuliso opo kene
 headerMode:'none'
}));


export default ASU;

/*YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);*/

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    // alignItems: 'center',
    //justifyContent: 'center',
    flex:1,
    flexDirection:'row',
    justifyContent:"space-around"
  },
});
