

export const Totalus = (state) => {
    const total = Object.values(state.true_cart.cart).reduce((acc, item) => {
        acc += (item.qua * item.topri)
        return acc
    }, 0)
    return total.toFixed(2)
}

