import {INCREASE_QUANTITY, DECREASE_QUANTITY ,SaveZ,Get_Userz,Nextz,Filtre,Add_To_Cart,Remove_From_Cart,Empty_Cart,Add_Order,Quan,Relocate_Umum} from './constantine'
import { omit } from 'lodash'

const initialState1 = {
    cart: [],
    total: 0,
    qty: 0,
}

// export const toal = (state) => {
//     const total = Object.values(...state.true_cart.cart).reduce((acc, item) => {
//         acc +=  item.topri
//         return acc
//     }, 0)
//     return total.toFixed(2)
// }

export default function (state = initialState1, action) {
    switch(action.type){
        case Quan:
           return{
            ...state,
            qty: state.qty,...action.playload
           };

        case Add_To_Cart:{
                // let find = state.cart.findIndex(el => el.item.id_produk == action.playload.item.id_produk);
                //    if(find == -1){ 
                //         return {
                //             ...state,
                //             cart: [...state.cart,action.playload],
                //             qty: state.cart.qty ,
                //             total: action.playload.item.harga_biji*state.cart.qty

                //         }
                //     }else {
                //         return {
                //             ...state,qty: state.cart.qty == state.cart.qty + 1
                //         }
                //     }

                let { id_produk, harga_biji , nama_produk, harga_grosir } = action.playload.item
                let { qua } = action.playload
                //let topri = qua * harga_biji
    
                // IF ITEM IS ALREADY IN THE CART ADD TO QUANTITY
                if (state.cart[id_produk]) {
                    return {
                        ...state,
                        cart: {
                          ...state.cart,
                              [id_produk]: {
                                id_produk,
                                qua: state.cart[id_produk].qua + qua,
                                nama_produk,
                                harga_biji,
                                topri: (state.cart[id_produk].qua + qua)  * harga_biji,
                                //topri: state.cart[id_produk].qua * harga_biji
                            }
                        }
                    }
                }
    
                return {
                    ...state,
                    cart: {
                       ...state.cart,
                           [id_produk]: {
                            id_produk,
                            nama_produk,
                            qua,
                            harga_biji,
                            topri: harga_biji,
                            //topri

                        }
                    }
                   
                }
            }  
        
        case Empty_Cart:
            return {
                ...state,
                cart: [],
                total: 0
            }
        case Remove_From_Cart:{
            // let { id_produk } = action.playload;
            // return{
            //     ...state,cart: state.cart.filter(car =>
            //         car.id_produk !== id_produk)
            // }

            // const newOrders =  state.cart.filter(item =>item.id_produk != action.playload.id_produk);          
            //     return {...state,cart: newOrders }
        
           
            let { id_produk } = action.playload.product;
            const updateRemoved = omit(state.cart,id_produk)

            return {
                ...state,cart: updateRemoved
            }
        }

        case INCREASE_QUANTITY: {
            let { id_produk, nama_produk , harga_biji , qua } = action.playload
            //let { qua } = action.playload

            return {
                ...state,
                cart: {
                    ...state.cart,
                    [id_produk]: {
                        id_produk,
                        nama_produk,
                        qua: state.cart[id_produk].qua + 1,
                        harga_biji,
                        topri: (state.cart[id_produk].qua + 1) * harga_biji
                    }
                }
            }
        }
        case DECREASE_QUANTITY: {
            let { id_produk, nama_produk , harga_biji , qua } = action.playload

            return{

            }
        }


        default:
        return { ...state}
    }
}