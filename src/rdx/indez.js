import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from 'redux-thunk';

import rootReducer from "./reducer";
import cartItems from './Quarantine/cart_reducer';
import cartReducer from './cartReducer';
import orderReducer from './orderReducer';
const middleware = [thunk];
const initialState = {};

const Anjing = combineReducers({norm :rootReducer, true_cart : cartReducer, true_order: orderReducer })
//const store = createStore(combineReducers({rootReducer,cartItems}), applyMiddleware(...middleware));
//const store = createStore(rootReducer, applyMiddleware(...middleware));
const store = createStore(Anjing, initialState, applyMiddleware(...middleware));
export default store;



// import { combineReducers } from  'redux';
// import productReducer from './productReducer';
// import cartReducer from './cartReducer';
// import orderReducer from './orderReducer';
// export default combineReducers({
//     products: productReducer,
//     cart: cartReducer,
//     order: orderReducer
// })