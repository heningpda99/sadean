
import {INCREASE_QUANTITY,DECREASE_QUANTITY,Nextz,Get_Userz,Filtre,Add_To_Cart,Remove_From_Cart,Empty_Cart,Add_Order,Quan,Relocate_Umum} from './constantine'



export const setZez = userz => ({
    type:Get_Userz,
    playload:userz
})

export const setNext = txen =>({
    type:Nextz,
    playload:txen
})

export const setFiltre = ertlif =>({
    type:Filtre,
    playload:ertlif
})


export const setRelocate = reloc =>({
    type:Relocate_Umum,
    playload:reloc
})

export const setQuan = (qu) =>({
    type:Quan,
    playload:qu
})




export function addToCart(item,qua){
    return{
        type:Add_To_Cart,
        playload:{item,qua}
    }
}

export function removeItem(product){
        return{   
                type:Remove_From_Cart,
                playload:{product}
        }
}

export function increaseQuantity(itm) {
    return {
        type:INCREASE_QUANTITY,
        playload:itm
    }
}

export function decreaseQuantity(item) {
    return {
        type:DECREASE_QUANTITY,
        playload:item 
        
    }
}


export const emptyCart = ()  => ({
    
        type:Empty_Cart
    
})

export const addOrder = data => ({

        type:Add_Order,
        playload:data
        
})

