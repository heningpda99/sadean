import React from 'react';
import {AsyncStorage, Alert, StyleSheet, Text, View ,YellowBox,StatusBar} from 'react-native';
import {createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import {Container,Content,Header,Button,Icon,Body,Left,Right} from 'native-base'
//import _confirm_logout from '../Indexo'
import * as Expo from 'expo'


export const _delete_all_data = async(props)=>{
  await AsyncStorage.removeItem('Sess')
  .then(res => {
      return props.navigation.navigate('Loin');
  })
}



class Profile extends React.Component {
  constructor(props){
    super(props)

  }

  async componentWillMount(){
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
  });
  }

  _confirm_logout(props){
    Alert.alert(
        'Konfirmasi',
        'Apakah Anda Yakin?',
        [
          {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Ya', onPress: () => _delete_all_data(props)},
        ],
        { cancelable: true }
      )
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#fff'}}>
          <Body>
            <View></View>
          </Body>
          <Right>
            <Icon name="ios-exit" style={{color:'#A3356A'}}  onPress={()=>this._confirm_logout(this.props)}/>
          </Right>
        </Header>
        <Content>
          {/* <Button block border alert onPress={()=>this._confirm_logout(this.props)}>
            <Icon name='ios-exit'/>
          </Button> */}
        </Content>
      </Container>
    );
  }
}
// const Ofil = createAppContainer(createStackNavigator({Profile}));
export default Profile;

/*YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
