import React from 'react';
import { StyleSheet, Text, View ,Button,YellowBox,StatusBar} from 'react-native';
import {createBottomTabNavigator,createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import Home from './Home/Home';
//import Holme from './Home/Home';
import Profile from './Profile/Profile';
import Kera_Riwa from './Kera_Riwa/Kera_Riwa';
import Message from './Message/Message';
import Icon from 'react-native-vector-icons/Ionicons';
// class App extends React.Component {
//   static navigationOptions = {
//     title: "Home"
//   };

//   render() {
//     return (
//       <View style={styles.container}>
//         <Text>Open up App.js to start working on your app!</Text>
//       </View>
//     );
//   }
// }

export const _delete_all_data = async(props)=>{
  await AsyncStorage.removeItem('Sess')
  .then(res => {
      return props.navigation.navigate('Loin');
  })
}

export const _confirm_logout=()=>{
  Alert.alert(
      'Konfirmasi',
      'Apakah Anda Yakin?',
      [
        {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Ya', onPress: () => _delete_all_data(this.props)},
      ],
      { cancelable: true }
    )
}

const Tab = createBottomTabNavigator({
  Home:{screen:Home,
        navigationOptions:{
          tabBarLabel:'Home',
          tabBarIcon:({tintColor})=>(
            <Icon name='ios-home' color={tintColor} size={24}/>
          )
        }},
  Kera_Riwa:{screen:Kera_Riwa,
        navigationOptions:{
            tabBarLabel:'Order',
            tabBarIcon:({tintColor})=>(
              <Icon name='ios-cart' color={tintColor} size={24}/>
            )  
        }},    
  Message:{screen:Message,
          navigationOptions:{
            tabBarLabel:'Message',
            tabBarIcon:({tintColor})=>(
              <Icon name='ios-mail' color={tintColor} size={24}/>
            )}
        },  
  Profile:{screen:Profile,
          navigationOptions:{
            tabBarLabel:'Profile',
            tabBarIcon:({tintColor})=>(
              <Icon name='ios-person' color={tintColor} size={24}/>
            )}
    },
   
},{
  navigationOptions:{
    //routes e Halaman
    initialRouteName:'Home',
    order:['Home','Kera_Riwa','Message','Profile'],
    //ngatur tab e ngisor
    tabBarVisible:true
  },
  tabBarOptions:{
    activeTintColor:'#A3356A',
    inactiveTintColor:'grey',
    showLabel:true,
    showIcon:true
  },
    tabBarSelectedItemStyle: {
      borderBottomWidth: 2,
      borderBottomColor: 'red',
  },
});

const Cok = createAppContainer(Tab);

export default Cok;


// const Paa = createAppContainer(createStackNavigator({App}));
// export default Paa;

/*YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
