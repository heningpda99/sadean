import React from 'react';
import { StyleSheet, Text, View ,Button,YellowBox,StatusBar} from 'react-native';
import {createBottomTabNavigator,createAppContainer,createStackNavigator,StackNavigator, DrawerNavigator, TabNavigator} from 'react-navigation';
import Home from './src/Home/Home';
import { Provider } from "react-redux";
import {Root} from 'native-base'
import store from './src/rdx/indez';
import Profile from './src/Profile/Profile';
import Icon from 'react-native-vector-icons/Ionicons';
import Tab from './src/Indexo';
import In from './src/Masok/Masok';
import Up from './src/Regis/Regis';
import Places from './src/Places/Places';
import Teracota from './src/Teras/Teras';
import HaPenDuk from './src/Home/HaPenDuk/HaPenDuk';

class App extends React.Component {

//   componentDidMount() {
//     StatusBar.setHidden(true);
//  }

//  componentWillUnmount() {
//   StatusBar.setHidden(false);
// }
    componentDidMount(){
      StatusBar.setBackgroundColor("#fff")
    }

  render() {
    //console.disableYellowBox= true;
    return (
      <Provider store={store} >
      <Root>
      <Cok/>
      </Root>
      </Provider>
    );
  }
}

const SNav = createStackNavigator({
  Terace:{screen:Teracota},
  Indexo:{screen:Tab},
  Perorin:{screen:Profile},
  Loin:{screen:In},
  Upzy:{screen:Up},
  Pla:{screen:Places}
},{
  initialRouteName:'Terace',
  headerMode:"none"
});

const Cok = createAppContainer(SNav)

//export default Cok;
 export default App;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
